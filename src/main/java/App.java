import com.ubuntucellular.user.UserController;
import com.ubuntucellular.util.Path;

import static spark.Spark.*;

public class App{

    public App(){
        int maxThreads = 80;
        int minThreads = 2;
        int timeOutMillis = 30000;
        ProcessBuilder processBuilder = new ProcessBuilder();
        Integer port;
        if(processBuilder.environment().get("PORT")!=null){
            port=Integer.parseInt(processBuilder.environment().get("PORT"));
        }else{
            port= Path.Env.PORT;
        }
        port(port);
        threadPool(maxThreads,minThreads,timeOutMillis);
        staticFileLocation("/public");
        options("/*",(request, response) -> {
            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if(accessControlRequestHeaders != null){
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }
            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if(accessControlRequestMethod != null){
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }
            return "OK";
        });
        before((request, response)->{
            response.header("Access-Control-Allow-Origin","*");
            response.header("Access-Control-Request-Method","GET,POST,PUT,UPDATE,DELETE,OPTIONS");
            response.header("Access-Control-Allow-Headers","Origin,X-Requested-With,Content-Type,Accept");
            response.type("application/json");
        });
        get(Path.Web.HOME, (request,response)-> {
            response.type(Path.Web.HTML_TYPE);
            response.status(200);
            return "<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width, user-scalable=no\">\n" +
                    "    <!-- UIkit CSS -->\n" +
                    "    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/css/uikit.min.css\" />\n" +
                    "\n" +
                    "    <!-- UIkit JS -->\n" +
                    "    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/js/uikit.min.js\"></script>\n" +
                    "    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/js/uikit-icons.min.js\"></script>\n" +

                    "    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.2/socket.io.min.js\"></script>\n" +
                    "\n" +
                    "    <title>True Caller Mobile Web SDK</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "\n" +
                    "<article class=\"uk-article\">\n" +
                    "\n" +
                    "    <h1 class=\"uk-article-title\"><a class=\"uk-link-reset\" href=\"\">True Caller Mobile Web Kit</a></h1>\n" +
                    "\n" +
                    "    <p class=\"uk-article-meta\">Written by <a href=\"#\">Setup</a> on 30 April 2019. By <a href=\"#\">Graham</a></p>\n" +
                    "\n" +
                    "    <p class=\"uk-text-lead\">This is a good experience. Try it by clicking on the button below</p>\n" +
                    "\n" +
                    "    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n" +
                    "\n" +
                    "    <div class=\"uk-grid-small uk-child-width-auto\" uk-grid>\n" +
                    "        <div>\n" +
                    "            <a id=\"truecaller\" onclick=\"return check()\" href=\"#\" class=\"uk-button uk-button-default\">Click Me!</a>\n" +
                    "        </div>\n" +
                    "    </div>\n" +
                    "\n" +
                    "</article>    \n" +
                    "\n" +
                    "    <script>\n" +
                    "function check(){\n" +
                    "\n" +
                    "window.location.href=\"truecallersdk://truesdk/web_verify?requestNonce=10000000&partnerKey=g3wt09341e977a4474640b8adcba35161df02&partnerName=trucallermobile&lang=en&title=verify&skipOption=skip\";\n" +
                    "\n" +
                    "var socket = io({query: `requestId='10000000'`});\n" +
                    "socket.on('sign-in-complete',profile =>{\n" +
                    " console.log(profile);\n" +
                    "});\n" +
                    "}" +
                    "    </script>\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>\n";
        });
        post(Path.Web.AUTH, UserController::handleAuthCallback);
        get(Path.Web.LOGIN,(request, response) -> {
            response.type(Path.Web.HTML_TYPE);
            response.status(200);
            return "<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width, user-scalable=no\">\n" +
                    "    <!-- UIkit CSS -->\n" +
                    "    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/css/uikit.min.css\" />\n" +
                    "\n" +
                    "    <!-- UIkit JS -->\n" +
                    "    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/js/uikit.min.js\"></script>\n" +
                    "    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/js/uikit-icons.min.js\"></script>\n" +
                    "    <script src=\"https://cdn.jsdelivr.net/npm/@iamdew/deep-link@latest/dist/deep-link.min.js\"></script>\n" +
                    "\n" +
                    "    <title>True Caller Mobile Web SDK</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "\n" +
                    "<form action=\"/login\" method=\"POST\" autocomplete=\"on\">\n" +
                    "    <fieldset class=\"uk-fieldset\">\n" +
                    "\n" +
                    "        <legend class=\"uk-legend\">Manual Login</legend>\n" +
                    "\n" +
                    "        <div class=\"uk-margin\">\n" +
                    "            <input class=\"uk-input\" type=\"number\" minlength=\"12\" placeholder=\"275700100200\">\n" +
                    "        </div>\n" +
                    "        <div class=\"uk-margin\">\n" +
                    "            <input class=\"uk-input\" type=\"text\" placeholder=\"First Name\">\n" +
                    "        </div>\n" +
                    "        <div class=\"uk-margin\">\n" +
                    "            <input class=\"uk-input\" type=\"text\" placeholder=\"Last Name\">\n" +
                    "        </div>\n" +
                    "        <div class=\"uk-margin\">\n" +
                    "            <input class=\"uk-input\" type=\"submit\" value=\"Login\">\n" +
                    "        </div>\n" +
                    "    </fieldset>\n" +
                    "</form>\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>\n";

        });
        post(Path.Web.LOGIN, UserController::handleLogin);
    }
    public static void main(String[] args){
        new App();
    }
}