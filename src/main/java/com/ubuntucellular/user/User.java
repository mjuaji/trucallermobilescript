package com.ubuntucellular.user;

public class User {
    private String requestId="";
    private String accessToken="";
    private String endpoint="";
    public User(){}
    public User(String requestId, String accessToken, String endpoint) {
        this.requestId = requestId;
        this.accessToken = accessToken;
        this.endpoint = endpoint;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getEndpoint() {
        return endpoint;
    }
}
