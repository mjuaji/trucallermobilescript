package com.ubuntucellular.util;


public class Path {

    public static class Web{
        public static final String JSON_TYPE ="application/json";
        public static final String HTML_TYPE ="text/html";
        public static final String HOME="/";
        public static final String AUTH="/auth";
        public static final String LOGIN="/login";
        //"truecallersdk://truesdk/web_verify?requestNonce=UNIQUE_REQUEST_ID&partnerKey=YOUR_APP_KEY&partnerName=YOUR_APP_NAME&lang=LANGUAGE_LOCALE&title=TITLE_STRING_OPTION&skipOption=FOOTER_CTA_STRING"
        public static final String DEEP_LINK="intent://truesdk/web_verify?requestNonce="+UniqueRequestID()+"&partnerKey="+Auth.TRUE_CALLER_APP_KEY+"&partnerName="+Auth.TRUE_CALLER_APP_NAME+"&lang="+Auth.TRUE_CALLER_LANGUAGE+"&title="+Auth.TRUE_CALLER_TITLE+"&skipOption="+Auth.TRUE_CALLER_SKIP_OPTION+";"+"scheme=truecallersdk;end";
    }

    public static class Auth{
        public static final String TRUE_CALLER_APP_KEY="g3wt09341e977a4474640b8adcba35161df02";
        public static final String TRUE_CALLER_APP_NAME="trucallermobile";
        public static final String TRUE_CALLER_LANGUAGE="en";
        public static final String TRUE_CALLER_TITLE="verify";
        public static final String TRUE_CALLER_SKIP_OPTION="skip#Intent";
    }

    public static String UniqueRequestID(){
        return String.valueOf(Math.random()*100000000);
    }

    public static class Env{
        public static final Integer PORT=9900;
    }
}
