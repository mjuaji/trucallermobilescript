# True Caller Mobile Web SDK
## Resources
  - [Heroku](https://heroku.com)
  - [Socket.io](https://socket.io/docs/#Using-it-just-as-a-cross-browser-WebSocket)

### Findings
Writing the plain JS library to handle deeplinking is not smooth and will not support:
  - NonJS Savvy Developers
  - Beginner Developers
  - Lazy/Time Constraint

It is a good idea to clone and repurpose/support the library & some introduction to deeplinking may be required for new developers.

Document can then be ammended to have the information on the Deep Link Library used even more elaborate.

### Opportunities
Instead of defaulting to just a URL or playstore for the user to download, other methods like voice calls that reject on pick up can be used.