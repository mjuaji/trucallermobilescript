enablePlugins(JavaAppPackaging)

name := "trucallerscripted"

organization := "com.ubuntucellular"

version := "1.0.0"

herokuAppName in Compile := "trucallerscripted"

javacOptions += "-Xlint:unchecked"

libraryDependencies += "org.json" % "json" % "20090211"

libraryDependencies += "com.sparkjava" % "spark-core" % "2.7.1"

libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.25"

libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.25"

libraryDependencies += "com.google.code.gson" % "gson" % "2.8.2"

libraryDependencies += "com.squareup.okhttp3" % "okhttp" % "3.14.1"

